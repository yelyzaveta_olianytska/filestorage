﻿using DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace DAL
{
    public class DbContext : IdentityDbContext<User>
    {
        public DbContext() : base("Connection")
        {

        }

        public DbSet<File> Files { get; set; }
        public DbSet<Directory> Directories { get; set; }
        public DbSet<UserDirectory> UserDirectories { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}
