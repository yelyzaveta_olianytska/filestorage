﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces
{
    public interface IRepository<T>
    {
        void Create(T item);
        void Delete(T item);
        void Update(T item);
    }
}
