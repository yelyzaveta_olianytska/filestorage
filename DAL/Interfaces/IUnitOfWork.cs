﻿using DAL.Entities;
using DAL.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        AppUserManager UserManager { get; }
        AppRoleManager RoleManager { get; }
        IRepository<UserProfile> UserProfile { get; }
        Task<int> SaveAsync();
    }
}
