﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class File : BaseEntity
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Link { get; set; }
        public string Path { get; set; }
        public int Size { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsRemove { get; set; }
        public DateTime Created { get; set; }
        public int DirectoryId { get; set; }
        public Directory Directory { get; set; }
    }
}
