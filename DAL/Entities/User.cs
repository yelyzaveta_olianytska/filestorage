﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class User : IdentityUser
    {
        public IEnumerable<UserDirectory> UserDirectories { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
