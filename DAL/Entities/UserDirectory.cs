﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class UserDirectory : BaseEntity
    {
        public int UserId { get; set; }
        public int DirectoryId { get; set; }
        public User User { get; set; }
        public Directory Directory { get; set; }
    }
}
