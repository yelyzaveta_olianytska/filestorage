﻿using DAL.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Identity
{
    public class AppRoleManager : RoleManager<UserRole>
    {
        public AppRoleManager(IRoleStore<UserRole, string> store) : base(store)
        {
        }
    }
}
