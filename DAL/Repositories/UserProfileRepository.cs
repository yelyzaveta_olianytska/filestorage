﻿using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class UserProfileRepository : IRepository<UserProfile>
    {
        private readonly DbContext context;
        public UserProfileRepository(DbContext context)
        {
            this.context = context;
        }
        public void Create(UserProfile item)
        {
            context.UserProfiles.Add(item);
            context.SaveChanges();
        }

        public void Delete(UserProfile item)
        {
            throw new NotImplementedException();
        }

        public void Update(UserProfile item)
        {
            throw new NotImplementedException();
        }
    }
}
