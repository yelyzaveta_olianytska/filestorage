﻿using DAL.Entities;
using DAL.Identity;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext context;
        public UnitOfWork(DbContext context)
        {
            this.context = context;
        }
        public AppUserManager UserManager => new AppUserManager(new UserStore<User>(context));

        public AppRoleManager RoleManager => new AppRoleManager(new RoleStore<UserRole>(context));

        public IRepository<UserProfile> UserProfile => new UserProfileRepository(context);

        public Task<int> SaveAsync()
        {
            return context.SaveChangesAsync();
        }
    }
}
